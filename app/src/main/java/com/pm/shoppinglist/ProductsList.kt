package com.pm.shoppinglist

class ProductsList {
    var listName: String;
    var listDate: String;

    constructor(listName: String, listDate: String) {
        this.listName = listName
        this.listDate = listDate
    }

    override fun toString(): String {
        return "ProductsList(listName='$listName', listDate='$listDate')"
    }


}
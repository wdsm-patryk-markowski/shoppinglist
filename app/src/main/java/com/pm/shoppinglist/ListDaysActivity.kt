package com.pm.shoppinglist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import javax.xml.validation.ValidatorHandler

class ListDaysActivity : AppCompatActivity() {
    var recreate = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_days)
        val listButton = findViewById<ImageButton>(R.id.listButton)
        val firebaseRef = FirebaseDatabase.getInstance().getReference("productsList")

        var days = listOf<String>();

        firebaseRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                for (postSnapshot in snapshot.children) {
                    days += postSnapshot.key.toString()
                }
                if(days != null) {
                    renderList(days)
                }
                if(recreate){
                    recreate()
                }

            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("loadPost:onCancelled", error.toException())
            }
        })

        listButton.setOnClickListener {
            super.onBackPressed();

        }


    }
    fun renderList(days : List<String>){
        val listDaysViewer = findViewById<ListView>(R.id.dateListListView)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, days)
        listDaysViewer.adapter = adapter

        listDaysViewer.setOnItemClickListener { parent, view, position, id ->
            val selectedItem = parent.getItemAtPosition(position) as String


            val intent = Intent(this, SpecificDayListActivity::class.java).apply {
                putExtra("date", selectedItem)
            }
            startActivity(intent)
            finish()
            overridePendingTransition(0,0);
        }
        listDaysViewer.onItemLongClickListener = AdapterView.OnItemLongClickListener { adapterView, view, i, l ->
            val firebaseRef = FirebaseDatabase.getInstance().getReference("productsList")
            firebaseRef.child(days[i]).removeValue()
            recreate= true
            true
        }
    }
    }




package com.pm.shoppinglist

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val textDate = findViewById<TextView>(R.id.mainDateText)
        val listButton = findViewById<ImageButton>(R.id.listButton)
        val addList = findViewById<ImageButton>(R.id.addListButton)

        textDate.text = "Witaj!"
        listButton.setOnClickListener {
            val myIntent = Intent(listButton.context, ListDaysActivity::class.java)

            startActivityForResult(myIntent, 0)
        }
        addList.setOnClickListener {

            val myIntent = Intent(addList.context, CreateNewListActivity::class.java)

            startActivityForResult(myIntent, 0)
        }

    }
}
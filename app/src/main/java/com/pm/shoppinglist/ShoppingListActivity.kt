package com.pm.shoppinglist

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class ShoppingListActivity : AppCompatActivity() {
    var listNames = listOf<Product>();
    var recreate = false
    var selectedDate = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_list)
        val date = intent.getStringExtra("date")
        if (date != null) {
            selectedDate = date
        }
        val nameOfList = intent.getStringExtra("nameOfList")
        val textDate = findViewById<TextView>(R.id.mainDateText)
        val listName = findViewById<TextView>(R.id.listNameText)
        val prevButton = findViewById<ImageButton>(R.id.prevButton)
        val extraProductButton = findViewById<ImageButton>(R.id.addExtraProduct)
        var firebaseRef = FirebaseDatabase.getInstance().getReference("productsList/$date/$nameOfList");

        textDate.text = date
        listName.text = nameOfList



        firebaseRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                for (postSnapshot in snapshot.children) {
                    val prod = Product(
                            postSnapshot.child("productName").value.toString(),
                            postSnapshot.child("productQty").value.toString().toInt(),
                            postSnapshot.child("productUnit").value.toString()
                    )
                    listNames += prod
                }
                if (date != null && nameOfList != null) {

                    renderList(listNames, date, nameOfList);

                }
                if(recreate)
                {
                    finish();
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }

            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("loadPost:onCancelled", error.toException())
            }

        })

        prevButton.setOnClickListener {
            onBackPressed();
        }
        extraProductButton.setOnClickListener {
            val intent = Intent(this, AddExtraProductActivity::class.java).apply {
                putExtra("date",date)
                putExtra("nameOfList",nameOfList)
            }
            startActivity(intent)
        }





    }
    override fun onBackPressed() {
        val myIntent = Intent(this, SpecificDayListActivity::class.java).apply {
            putExtra("date", selectedDate)
        }

        startActivityForResult(myIntent, 0)
        finish()
        overridePendingTransition(0,0);

    }

    fun listProductsToString(nameList: List<Product>): List<String> {
        var nameListString: List<String> = listOf()
        for (item in nameList) {
            nameListString += item.toString()
        }

        return nameListString
    }

    fun renderList(nameList: List<Product>, date: String, nameOfList: String){
        val listOfProducts = findViewById<ListView>(R.id.listOfProductsView)

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listProductsToString(nameList))
        listOfProducts.adapter = adapter
        listOfProducts.onItemLongClickListener = AdapterView.OnItemLongClickListener { adapterView, view, i, l->
            val firebaseRef = FirebaseDatabase.getInstance().getReference("productsList/$date/$nameOfList");
            firebaseRef.child(listNames.get(i).productName).removeValue()
            recreate= true
            true



        }
    }




}




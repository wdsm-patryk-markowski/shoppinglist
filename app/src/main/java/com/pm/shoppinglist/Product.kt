package com.pm.shoppinglist

class Product {
    var productName: String;
    var productQty: Int;
    var productUnit: String;

    constructor(productName: String, productQty: Int, productUnit: String) {
        this.productName = productName
        this.productQty = productQty
        this.productUnit = productUnit
    }

    override fun toString(): String {
        return productName + " " + productQty + " " + productUnit;
    }

}
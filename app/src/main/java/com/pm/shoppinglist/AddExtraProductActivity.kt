package com.pm.shoppinglist

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.core.operation.Merge
import java.lang.Exception

class AddExtraProductActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val date = intent.getStringExtra("date")
        val nameOfList = intent.getStringExtra("nameOfList")
        setContentView(R.layout.add_extra_product)
        val addNewProductButton = findViewById<Button>(R.id.addNewProductToList)
        val productName = findViewById<TextInputLayout>(R.id.productNameLayout)
        val productQty = findViewById<TextInputLayout>(R.id.productQtyLayout)
        val unitSpinner = findViewById<Spinner>(R.id.unitSpinner)
        val listName = findViewById<TextView>(R.id.listNameText)
        var firebaseRef = FirebaseDatabase.getInstance().getReference("productsList/$date/$nameOfList");

        listName.text = nameOfList

        ArrayAdapter.createFromResource(
                this,
                R.array.units,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            unitSpinner.adapter = adapter
        }

        fun addToDataBase(prod: Product) {
            val prodHash = HashMap<String, Product>();
            prodHash.put(prod.productName, prod)

            firebaseRef.updateChildren(prodHash as Map<String, Any>)

        }

        addNewProductButton.setOnClickListener {
            try {
                val product = Product(productName.editText?.text.toString(),
                        Integer.parseInt(productQty.editText?.text.toString()),
                        unitSpinner.selectedItem.toString())

                addToDataBase(product);
                val intent = Intent(this, ShoppingListActivity::class.java).apply {
                    putExtra("date", date)
                    putExtra("nameOfList", nameOfList)
                }
                startActivity(intent)
                finish()
                overridePendingTransition(0,0);
            }catch (e: Exception){
                Toast.makeText(this, "All fields must be field!",
                        Toast.LENGTH_LONG).show();
            }


        }


    }
}
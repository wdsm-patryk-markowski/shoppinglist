package com.pm.shoppinglist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class SpecificDayListActivity : AppCompatActivity() {
    var recreate = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_specific_date_list)
        val textDate = findViewById<TextView>(R.id.mainDateText)
        val listButton = findViewById<ImageButton>(R.id.listButton)
        val homeButton = findViewById<ImageButton>(R.id.homeButton)
        val date = intent.getStringExtra("date")
        val firebaseRef = FirebaseDatabase.getInstance().getReference("productsList/$date")

        var listNames = listOf<String>();

        firebaseRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (postSnapshot in snapshot.children) {
                    listNames += postSnapshot.key.toString()

                }
                if (date != null) {
                    renderList(listNames,date)
                }
                if(recreate){
                    recreate()
                }

            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("loadPost:onCancelled", error.toException())
            }
        })


        textDate.text = date
        listButton.setOnClickListener {
            val myIntent = Intent(listButton.context, ListDaysActivity::class.java)

            startActivityForResult(myIntent, 0)
        }

        homeButton.setOnClickListener {
            val myIntent = Intent(homeButton.context, MainActivity::class.java)
            startActivityForResult(myIntent, 0)
            finish()
            overridePendingTransition(0,0);
        }


    }

    override fun onBackPressed() {
        val myIntent = Intent(this, ListDaysActivity::class.java)

        startActivityForResult(myIntent, 0)
        finish()
        overridePendingTransition(0,0);

    }

    fun renderList(nameList : List<String>,date:String){
        val nameDaysViewer = findViewById<ListView>(R.id.nameListListView)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nameList)
        nameDaysViewer.adapter = adapter

        nameDaysViewer.setOnItemClickListener { parent, view, position, id ->
            val selectedItem = parent.getItemAtPosition(position) as String


            val intent = Intent(this, ShoppingListActivity::class.java).apply {
                putExtra("date", date)
                putExtra("nameOfList", selectedItem)
            }
            startActivity(intent)
            finish()
            overridePendingTransition(0,0);


        }
        nameDaysViewer.onItemLongClickListener = AdapterView.OnItemLongClickListener { adapterView, view, i, l ->
            val firebaseRef = FirebaseDatabase.getInstance().getReference("productsList/$date")
            firebaseRef.child(nameList[i]).removeValue()
            recreate= true
            true
        }
    }
}
package com.pm.shoppinglist

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isNotEmpty
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.database.FirebaseDatabase
import java.lang.Integer.parseInt
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.HashMap


class CreateNewListActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("ResourceType")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_new_list)
        val homeButton = findViewById<ImageButton>(R.id.homeButton)
        val createNewListButton = findViewById<Button>(R.id.createListButton)
        val addNewProductButton = findViewById<Button>(R.id.addNewProductToList)
        val listName = findViewById<TextInputLayout>(R.id.listNameAddLayout)
        val productName = findViewById<TextInputLayout>(R.id.productNameLayout)
        val productQty = findViewById<TextInputLayout>(R.id.productQtyLayout)
        val unitSpinner = findViewById<Spinner>(R.id.unitSpinner)
        val productsListView = findViewById<ListView>(R.id.productsListListView)

        val firebaseRef = FirebaseDatabase.getInstance().getReference("productsList")

        var products: List<Product> = mutableListOf();

        ArrayAdapter.createFromResource(
                this,
                R.array.units,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            unitSpinner.adapter = adapter
        }


        homeButton.setOnClickListener {
            val myIntent = Intent(homeButton.context, MainActivity::class.java)
            startActivityForResult(myIntent, 0)
            finish()
            overridePendingTransition(0,0);
        }

        fun addToDatabase(listDetails : ProductsList) {
            val datRef = firebaseRef.child(listDetails.listDate+"/"+listDetails.listName)
            val prod =  HashMap<String,Product>();
            for(item in products)
            {

              prod.put(item.productName,item)
            }
            datRef.setValue(prod)
        }

        createNewListButton.setOnClickListener {
            if(products.size != 0){
            if (listName.editText?.text.toString() != ""){
                var nameOfList = listName.editText?.text.toString()
                var datenow = LocalDateTime.now().toString().take(10);
                var prodList = ProductsList(nameOfList, datenow)
                addToDatabase(prodList);

                val intent = Intent(this, ShoppingListActivity::class.java).apply {
                    putExtra("date", datenow)
                    putExtra("nameOfList", nameOfList)
                }
                startActivity(intent)
                finish()
                overridePendingTransition(0,0);
            }
            else{
                Toast.makeText(this, "You must enter the name of the list!",
                        Toast.LENGTH_LONG).show();

            }}
            else{
                Toast.makeText(this, "You must enter product!",
                        Toast.LENGTH_LONG).show();
            }


        }


        fun lockingDataChange() {
            listName.editText?.isFocusableInTouchMode = false;
            listName.editText?.isFocusable = false;
        }


        fun stringListofProducts(list: List<Product>): List<String> {
            var products: List<String> = mutableListOf();
            for (item in list) {
                products += item.toString();
            }
            return products;
        }




        addNewProductButton.setOnClickListener {
            try {

                if(listName.editText?.text.toString() != "") {
                    lockingDataChange()
                }
                products += Product(productName.editText?.text.toString(),
                        parseInt(productQty.editText?.text.toString()),
                        unitSpinner.selectedItem.toString())

                val prodAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, stringListofProducts(products))

                productsListView.adapter = prodAdapter

                productName.editText?.text?.clear()
                productQty.editText?.text?.clear()
            }catch (e:Exception){
                Toast.makeText(this, "All fields must be field!",
                        Toast.LENGTH_LONG).show();
            }

        }


    }
}